package edu.bu.ec504.spr24.brain;

import edu.bu.ec504.spr24.brain.Board.Pos;
import edu.bu.ec504.spr24.sameGameTris.CircleColor;

import javax.swing.*;
import java.util.LinkedList;

/**
 * A smarter brain, for you to produce.
 */
public class SmarterBrain extends Brain {

    private Board currBoard;
    public SmarterBrain() {
        super();
    }

    @Override
    public String myName() {
        return null;
    }

    @Override
    Pos nextMove() {
        currBoard = new Board();

        for (int xx = 0; xx < myGUI.boardWidth(); xx++)
            for (int yy = 0; yy < myGUI.boardHeight(); yy++)
                currBoard.modify(xx, yy, myGUI.colorAt(xx, myGUI.boardHeight() - yy - 1));

        return chooseMove();
    }

    private Pos chooseMove() {
        Pos bestPos = new Pos(0, 0);
        Board currStateCopy = new Board(currBoard);

        // first, find column with greatest height
        int maxColHeight = 0;
        LinkedList<Integer> maxCols = new LinkedList<>();

        // iterate through each column
        for(int ii = 0; ii < currStateCopy.columns(); ii++){
            // calculate # of circles in the column
            int h = 0;
            for(int jj = 0; jj < currStateCopy.rows(ii); jj++){
                if(currStateCopy.getAt(ii, jj) != CircleColor.NONE){
                    h++;
                }
            }

            // if equal # of circles, add to list, else, clear list and update maxHeight
            if(h == maxColHeight){
                maxCols.add(ii);
            }
            if(h > maxColHeight){
                maxColHeight = h;   // update max column height (max # of circles)
                maxCols.clear();
                maxCols.add(ii);
            }
        }


        // then, choose option that has greatest score increase
        int maxScore = 0;
        for(Integer i : maxCols){
            System.out.println(i);
            for (int j = 0; j < currBoard.rows(i); j++) {
                if (currStateCopy.getAt(i, j) != CircleColor.NONE) {
                    Board test = new Board(currStateCopy);

                    // mark all other nodes in the region as "clear" (but does not delete anything)
                    currStateCopy.clickNodeHelper(i, j, test.getAt(i, j));

                    // try removing the region to see what is left over
                    int count = test.clickNode(i, j);
                    if (count > maxScore) {
                        // record a new best move
                        maxScore = count;
                        bestPos = new Board.Pos(i, myGUI.boardHeight()-1-j);
                    }

                }
            }
        }

        return bestPos;
    }

}
